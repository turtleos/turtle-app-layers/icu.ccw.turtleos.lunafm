export const TRANSLATIONS = {
    navigation: {
        back: {
            de: 'Aufwärts',
            en: 'Back'
        },
        forward: {
            de: 'Abwärts',
            en: 'Forward'
        },
        reload: {
            de: 'Aktualisieren',
            en: 'Refresh'
        },
        edit: {
            de: 'Pfad ändern',
            en: 'Change Path'
        }
    },
    actions: {
        create: {
            de: 'Neu',
            en: 'New'
        },
        delete: {
            de: 'Löschen',
            en: 'Delete'
        }
    },
    types: {
        file: {
            en: 'File',
            de: 'Datei'
        },
        folder: {
            en: 'Folder',
            de: 'Ordner'
        }
    },
    errors: {
        default: {
            de: 'Ein Fehler trat auf',
            en: 'An Error occured'
        },
        foldernotfound: {
            de: 'Pfad nicht gefunden',
            en: 'Path couln\'t be found'
        },
        readfailed: {
            de: 'Konnte Ordner nicht lesen',
            en: 'Failed to read Folders content'
        }
    },
    welcome: {
        title: {
            de:'Willkommen',
            en: 'Welcome'
        },
        text: {
            de: 'Luna ist ein minimalistischer Dateiexplorer',
            en: 'Luna is a minimalistic Fileexplorer'
        }
    }
}
