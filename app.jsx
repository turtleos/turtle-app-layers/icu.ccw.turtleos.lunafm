import React from 'react';

import { AppLayer } from '../../turtleos-am/am';
import {PERMISSION} from '../../../lib/turtleos-permissions/permissions';

import {TRANSLATIONS} from './translations.js';
import icon from './icon.png';
import fileicon from './file.png';
import foldericon from './folder.png';

function MenuBarItem({onClick, colorScheme={text:'#fff',bg:['#000','#eee']}, icon=<br/>, tooltip=''}) {
  const [hovered, setHovered] = React.useState(false);
  return (
    <div style={{
      borderRadius: '50%',
      background: colorScheme.bg[1>>!hovered],
      transition: 'background .4s',
      width: '1.6rem',
      height: '1.6rem',
      padding: '0.4rem',
      margin: '0.2rem',
      color: colorScheme.text,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'relative'
    }} onClick={onClick}
         onMouseEnter={()=>setHovered(true)}
         onMouseLeave={()=>setHovered(false)}>
      {icon}
      <div style={{
        transform: `scale(${hovered?1:0}) translate(2rem, -50%)`,
        transition: 'transform .4s',
        borderRadius: '6px',
        position: 'absolute',
        top: '50%',
        height: '1.6rem',
        padding: '0.2rem',
        left: 0,
        background: '#000',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
        zIndex: 10000,
        width: 'max-content'
      }}>{tooltip}</div>
    </div>
  );
}

function MenuBar({appdk, onEvent, mrtranslate}) {
  const Feather = appdk.UI.ICONS.Feather;
  const [path, setPATH] = React.useState('/home');

  const buttonConfig = {
    back: {
      icon: <Feather.ChevronLeft/>,
      tooltip: 'navigation.back',
      onClick: ()=>{onEvent('navigate:back')}
    },
    forward: {
      icon: <Feather.ChevronRight/>,
      tooltip: 'navigation.forward',
      onClick: ()=>onEvent('navigate:forward')
    },
    refresh: {
      icon: <Feather.Repeat/>,
      tooltip: 'navigation.reload',
      onClick: ()=>onEvent('view:refresh')
    },
    editPath: {
      icon: <Feather.Edit/>,
      tooltip: 'navigation.edit',
      onClick: ()=>onEvent('navigate:changePath', {
        path: '/home' // TODO make intercative
      })
    }
  };

  return (
    <div style={{
      width: '100%',
      height: '3rem',
      background: appdk.UI.COLORS.GRUVBOX.black_dark[0],
      overflowX: 'auto',
      overflowY: 'hidden',
      display:'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'left',
      position: 'absolute',
      top: 0,
      left: 0
    }}>
    {Object.entries(buttonConfig).map(([key, {icon, tooltip, onClick}]) => {
      return (<MenuBarItem onClick={onClick} icon={icon} tooltip={mrtranslate.translate(tooltip)} colorScheme={{
        text: appdk.UI.COLORS.GRUVBOX.white_dark[3],
        bg: [appdk.UI.COLORS.GRUVBOX.black_dark[0], appdk.UI.COLORS.GRUVBOX.black_dark[1]]
      }}/>);
    })}
    </div>
  );
}

function PopUp({title, text, icon}) {
  const [shown, setShown] = React.useState(false);

  React.useEffect(()=>{
    setTimeout(()=>{
      setShown(true);
    },300);
    setTimeout(()=>{
      setShown(false);
    }, 3000)
  },[]);

  return (
    <div style={{
      display:'flex',
      flexDirection: 'column',
      flexWrap: 'wrap',
      minHeight: '5rem',
      alignItems: 'start',
      justifyContent: 'center',
      right: 0,
      transform: `translateX(${shown?'-0.8rem':'120%'})`,
      position:'absolute',
      color: '#efefef',
      transition: 'transform .4s',
      bottom: '0.8rem',
      padding: '0.4rem',
      paddingLeft: '3rem',
      paddingRight: '0.8rem',
      borderRadius: '8px',
      background: '#0e0e0e'
    }}>
      <div style={{
        height: '100%',
        position:'absolute',
        left:'0.5rem',
        width:'2rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}>{icon}</div>
      <b style={{
        width: 'max-content'
      }}>{title}</b>
      <p style={{
        width:'max-content',
        margin:0
      }}>{text}</p>
    </div>
  );
}

function FileFolderView({appdk, onEvent, cache, dir}) {
  return (<div style={{
    width: '100%',
    height: '100%',
    position:'absolute',
    top: '3rem',
    left:0
  }}><appdk.UI.Scrollbars><div style={{
    width: '90%',
    marginLeft: '5%',
    display:'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  }}>
          {cache.map(({type, name, props})=>{
            return (<div style={{
              display:'flex',
              flexDirection: 'column',
              color: '#efefef',
              width: 'min-content',
              height: 'min-content',
              textAlign: 'center',
              margin: '0.4rem'
            }} onClick={()=>{
              if(type==='folder') {
                onEvent('navigate:changePath', {path: dir+'/'+name})
              }
            }}>
                      <img src={(type==='file')?fileicon:foldericon} alt={'ICON'} style={{
                        width: '5rem'
                      }}/>
                      {name}
                    </div>);
          })}
          </div>
          </appdk.UI.Scrollbars></div>);
}

function App({appdk, intent, bridge}) {

  const mrtranslate = new appdk.Translator(TRANSLATIONS);
  const FSTools = appdk.FS.FSTools;

  const [history, setHistory] = React.useState({history:['/home']});
  const [popups, setPopups] = React.useState({popups:[]});
  const [cache, setCache] = React.useState({cache:false});
  const [index, setIndex] = React.useState({index:0});

  const onEvent = (event, data={}) => {
    switch (event) {
      case 'navigate:back':
        let newIndex = index.index-1;
        if(newIndex<0) newIndex=0;
        onEvent('view:refresh')
        setIndex({index:newIndex});
        break;
      case 'navigate:forward':
        let newIndexf = index.index+1;
        if(newIndexf>=history.history.length) newIndexf=history.history.length-1;
        onEvent('view:reload')
        setIndex({index:newIndexf});
        break;
      case 'navigate:changePath':
        let cpath = FSTools.convertPath('', data.path||'/home');
        let d = FSTools.isFolder(cpath);
        if(d.exists) {
          let hist = history.history.slice();
          let ind = index.index;
          hist.push(cpath);
          ind=hist.length-1;
          setHistory({history:hist});
          setIndex({index: ind});
          onEvent('view:refresh', {path: cpath});
        } else {
          onEvent('popup:error', {
            text: mrtranslate.translate('errors.foldernotfound')
          })
        }
        break;
      case 'view:refresh':
        setCache({cache: false});
        let p = data.path || history.history[index.index];
        let dr = appdk.System.Settings.getVFS().readFolderSync(p);
            if(!dr.error) {
              let ff=Object.entries(dr.data.content).map(([name, {type, props}]) => {return {name: name, type:type, props:props};});
                setCache({cache: ff});
            } else {
                 onEvent('popup:error', {
                   text: mrtranslate.translate('errors.readfailed')
                 })
            }
        break;
        case 'popup:custom':
        let ups = popups.popups.slice();
        ups.push(<PopUp title={data.title} text={data.text} icon={data.icon}/>);
        setPopups({
          popups: ups
        });
        break;
        case 'popup:error':
        onEvent('popup:custom', {
          title: mrtranslate.translate('errors.default'),
          text: data.text,
          icon: <appdk.UI.ICONS.Feather.XCircle/>
        });
        break;
        default:
        onEvent('popup:custom', {
          title: data.title || mrtranslate.translate('welcome.title'),
          text: data.text || mrtranslate.translate('welcome.text'),
          icon: data.icon || <appdk.UI.ICONS.Feather.Moon/>
        })
    }
  }

  React.useEffect(()=>{
    if(history.history[index.index]) {
      onEvent('view:refresh')
    }
  },[])

  return (
    <div style={{
      position:'absolute',
      top:0,
      left:0,
      width:'100%',
      height:'100%',
      display:'flex',
      flexDirection:'column',
      justifyContent:'center',
      alignItems:'center'
    }}>
      <MenuBar appdk={appdk} onEvent={onEvent} mrtranslate={mrtranslate}/>
      {(!Array.isArray(cache.cache))?(<div style={{
        width: '100%',
        height: '100%',
        display:'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
      }}>
       <appdk.UI.ICONS.Feather.Loader color={'#efefef'}/>
                                      </div>):<FileFolderView appdk={appdk} cache={cache.cache} onEvent={onEvent} dir={history.history[index.index]}/>}
      {popups.popups}
    </div>
  );
}

const LAYER = new AppLayer('Luna', 'icu.ccw.turtleos.lunafm',{
    render: ( {intent={},bridge, appdk})=>{return (<App appdk={appdk} intent={intent} bridge={bridge}/>);},
    icon: icon,
    requests: {
        [PERMISSION.ROOT]:true,
    },
    forceGrant: {
        [PERMISSION.ROOT]:true
    }
});

export {
    LAYER
}
